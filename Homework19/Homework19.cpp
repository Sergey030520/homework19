﻿#include <iostream>

enum animals
{
    CAT,
    DOG,
    FOX, 
    WOLF,
};

class  Animal {
private:
    int age, dangerous;
    std::string type;
public:
    Animal(int copy_age, int copy_danger, int copy_type) : age(copy_age), dangerous(copy_danger) {
        switch (copy_type) {
            case CAT:
                type = "Cat";
                break;
            case DOG:
                type = "Dog";
                break;
            case FOX:
                type = "Fox";
                break;
            case WOLF:
                type = "Wolf";
                break;
        };
    }
    virtual void Voice() {
        std::cout << "Voice!" << std::endl;
    }
    int getDangerous() { return dangerous; }
    int getAge() { return age; }
    std::string getType() { return type; }
};
class Fox : public Animal {
public:
    Fox(int copy_age, int copy_danger) : Animal(copy_age, copy_danger, FOX){}
    void Voice() override{
        std::cout << "yap-yap-yap, yur-yuu!" << std::endl;
    }
};
class Pets {
private:
    std::string name;
protected:
    Pets(std::string newName) : name(newName) {};
public:
    std::string getName(){
        return name;
    }
};
class Dog : public Animal, public Pets {
public:
    Dog(int copy_age, int copy_danger, std::string newName) : Animal(copy_age, copy_danger, DOG), Pets(newName) {}
    void Voice() override {
        std::cout << "wow-wow-wow!" << std::endl;
    }
};
class Wolf : public Animal {
public:
    Wolf(int copy_age, int copy_danger) : Animal(copy_age, copy_danger, WOLF) {}
    void Voice() override {
        std::cout << "oooo ahhh!" << std::endl;
    }
};
class Cat : public Animal, public Pets {
public:
    Cat(int copy_age, int copy_danger, std::string newName) : Animal(copy_age, copy_danger, CAT), Pets(newName) {}
    void Voice() override {
        std::cout << "meow-meow-meow!" << std::endl;
    }
};




int main()
{
    Animal** animals = new Animal*[4];
    animals[0] = new Cat(1,2, "22");
    animals[1] = new Dog(1, 2, "23");
    animals[2] = new Fox(1, 2);
    animals[3] = new Wolf(1, 2);
    for (int i = 0; i < 4; ++i) {
        std::cout << animals[i]->getType() << ": ";
        animals[i]->Voice();
    }
}